package com.actech.recyclerview_retrofit2.network

import com.actech.recyclerview_retrofit2.model.Photo
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApiInterface {

    @GET("/v0/b/fashionpointgarments-216416.appspot.com/o/sample.json?alt=media&token=1421055c-1844-4db1-a1c4-5f55e1ec95d0")
    fun getPhotos() : Call<List<Photo>>

    companion object {

        var BASE_URL = "https://firebasestorage.googleapis.com"

        fun create() : ApiInterface {

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(ApiInterface::class.java)

        }
    }
}
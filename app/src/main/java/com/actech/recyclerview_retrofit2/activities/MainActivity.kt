package com.actech.recyclerview_retrofit2.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.actech.recyclerview_retrofit2.model.Photo
import com.actech.recyclerview_retrofit2.R
import com.actech.recyclerview_retrofit2.adapter.RecyclerAdapter
import com.actech.recyclerview_retrofit2.network.ApiInterface


import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    lateinit var recyclerView: RecyclerView
    lateinit var recyclerAdapter: RecyclerAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView = findViewById(R.id.customRecyclerView)
        recyclerAdapter = RecyclerAdapter(this)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = recyclerAdapter


        val apiInterface = ApiInterface.create().getPhotos()

        //apiInterface.enqueue( Callback<List<Photo>>())
        apiInterface.enqueue( object : Callback<List<Photo>>{
            override fun onResponse(call: Call<List<Photo>>?, response: Response<List<Photo>>?) {

                if(response?.body() != null)
                    recyclerAdapter.setPhotoListItems(response.body()!!)
            }

            override fun onFailure(call: Call<List<Photo>>?, t: Throwable?) {

            }
        })
    }
}